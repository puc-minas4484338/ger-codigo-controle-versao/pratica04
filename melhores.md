# Os 3 melhores filmes

<b>O Poderoso Chefão</b><p>
É uma história épica sobre uma família de mafiosos italianos e seu líder, interpretado por Marlon Brando.<p>
<img src=imagens/poderosochefao.jpg>
<br></br>


<b>Cidadão Kane</b><p>
Conta a história de um magnata da mídia fictício, Charles Foster Kane.<p>
<img src=imagens/Kane.jpg>
<br></br>


<b>Um Sonho de Liberdade</b><p>
É uma história de esperança, amizade e redenção ambientada em uma prisão.<p>
<img src=imagens/sonho.jpg>


